package me.gooz.alphabeta.util;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import me.gooz.alphabeta.R;

public final class Util {

    private static final ArgbEvaluator ARGB_EVALUATOR = new ArgbEvaluator();
    private static final JsonFactory JSON_FACTORY = new JsonFactory();

    // colors
    public static int evaluate(float f, int a, int b) {
        return (int) Util.ARGB_EVALUATOR.evaluate(f, a, b);
    }

    // compare
    public static int compare(long a, long b) {
        return (a > b) ? (1) : (a < b ? -1 : 0);
    }
    public static int compareAll(int... comparisons) {
        for (int i = 0; i < comparisons.length - 1; i++)
            if (comparisons[i] != 0)
                return comparisons[i];

        return comparisons[comparisons.length - 1];
    }

    // dimensions
    public static float convertDpToPx(float dp) {
        return dp * Resources.getSystem().getDisplayMetrics().density;
    }
    public static float convertPxToDp(float px) {
        return px / Resources.getSystem().getDisplayMetrics().density;
    }

    // equals
    public static boolean nullOrEquals(Object a, Object b) {
        if (a == null)
            return b == null;
        else
            return b != null && a.equals(b);
    }

    // interpolators
    public static final Interpolator INTERPOLATOR_ACCELERATE = new AccelerateInterpolator();
    public static final Interpolator INTERPOLATOR_LINEAR = new LinearInterpolator();

    // json
    public static JsonParser createParser(String url) throws IOException {
        return Util.JSON_FACTORY.createParser(new URL(url));
    }
    public static boolean skipTo(JsonParser parser, JsonToken token) throws IOException {
        while (parser.nextToken() != token)
            if (parser.currentToken() == null)
                return false;

        return true;
    }
    public static boolean skipTo(JsonParser parser, String field) throws IOException {
        while (!field.equals(parser.nextFieldName()))
            if (parser.currentToken() == null)
                return false;

        return true;
    }

    // resources
    public static final long ANIMATION_TIME_SHORT  = Resources.getSystem().getInteger(android.R.integer.config_shortAnimTime);
    public static final long ANIMATION_TIME_MEDIUM = Resources.getSystem().getInteger(android.R.integer.config_mediumAnimTime);
    public static final long ANIMATION_TIME_LONG   = Resources.getSystem().getInteger(android.R.integer.config_longAnimTime);
    public static int getAccentColor(Context context) {
        TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent, value, true);
        return value.data;
    }
    public static int getDarkPrimaryColor(Context context) {
        TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimaryDark, value, true);
        return value.data;
    }

    // text
    public static final Typeface FONT_ROBOTO_LIGHT = Typeface.create("sans-serif-light", Typeface.NORMAL);
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    public static String toString(List list) {
        return Arrays.toString(list.toArray());
    }

}
