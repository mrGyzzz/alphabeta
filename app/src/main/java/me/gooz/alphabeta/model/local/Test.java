package me.gooz.alphabeta.model.local;

import org.joda.time.DateTime;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("unused")
public class Test {

    private String ID, name, description, author;
    private DateTime timeCreated, timeModified;
    private final List<String> subjects;
    private final List<Question> questions;

    public Test() {
        this.subjects   = new LinkedList<>();
        this.questions  = new LinkedList<>();
    }

    public String getID() {
        return this.ID;
    }
    public String getName() {
        return this.name;
    }
    public String getDescription() {
        return this.description;
    }
    public String getAuthor() {
        return this.author;
    }
    public DateTime getTimeCreated() {
        return this.timeCreated;
    }
    public DateTime getTimeModified() {
        return this.timeModified;
    }
    public List<String> getSubjects() {
        return this.subjects;
    }
    public List<Question> getQuestions() {
        return this.questions;
    }

    public void setID() {
        this.ID = UUID.randomUUID().toString();
    }
    public void setID(String ID) {
        this.ID = ID;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public void setTimeCreated(DateTime timeCreated) {
        this.timeCreated = timeCreated;
    }
    public void setTimeModified(DateTime timeModified) {
        this.timeModified = timeModified;
    }

}
