package me.gooz.alphabeta.model.woordjesleren;

import android.os.Parcel;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.gooz.alphabeta.util.Util;

public class Category extends Item {

    Category(long id, String text) {
        super(id, text);
    }

    public String toString() {
        return super.toString().replaceFirst("Item", "Category");
    }
    //region REGION - Request Functionality
    public static List<Category> requestAll() throws IOException {
        JsonParser parser = Item.createParser("select_categories.php?");

        List<Category> categories = new ArrayList<>();
        Util.skipTo(parser, JsonToken.START_ARRAY);
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            parser.nextToken();

            long id = parser.nextLongValue(0);
            parser.nextToken();

            String text = parser.nextTextValue();
            parser.nextToken();

            Category category = new Category(id, text);
            categories.add(category);
        }

        parser.close();
        return categories;
    }
    public List<Book> request() throws IOException {
        JsonParser parser = Item.createParser("select_books.php?category=" + Long.toString(this.id) + "&");

        List<Book> books = new ArrayList<>();
        Util.skipTo(parser, JsonToken.START_ARRAY);
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            parser.nextToken();

            long id = parser.nextLongValue(0);
            parser.nextToken();

            String text = parser.nextTextValue();
            parser.nextToken();

            long listCount = parser.nextLongValue(0);
            parser.nextToken();

            Book book = new Book(id, text, listCount);
            books.add(book);
        }

        parser.close();
        return books;
    }
    //endregion
    //region REGION - Parcelable Implementation
    protected Category(Parcel in) {
        super(in);
    }
    public static final Creator<Category> CREATOR = new Creator<Category>() {
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
    //endregion

}
