package me.gooz.alphabeta.model.local;

import android.util.Xml;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

public final class TestParser {

    private static final DateTimeFormatter XML_TIME_FORMAT = DateTimeFormat.forPattern("HH:mm:ss.SSS dd/MM/yyyy Z").withOffsetParsed();

    private static final String XML_NAMESPACE      = "";
    private static final String XML_ROOT           = "test";
    private static final String XML_ID             = "id";
    private static final String XML_NAME           = "text";
    private static final String XML_DESCRIPTION    = "description";
    private static final String XML_CREATOR        = "creator";
    private static final String XML_TIME_CREATED   = "created";
    private static final String XML_TIME_MODIFIED  = "modified";
    private static final String XML_SUBJECT        = "subject"; // also attribute
    private static final String XML_QUESTION       = "question";
    private static final String XML_ANSWER         = "answer";
    private static final String XML_SYNONYM        = "synonym";
    private static final String XML_NOTE           = "note";

    private static void skip(XmlPullParser parser) throws IOException, XmlPullParserException {
        for (int depth = 1; depth > 0; ) {
            if (parser.next() == XmlPullParser.START_TAG) depth++;
            else if (parser.getEventType() == XmlPullParser.END_TAG) depth--;
        }
    }
    private static String read(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, XML_NAMESPACE, tag); // pretty much unnecessary
        String text = parser.nextText();
        parser.require(XmlPullParser.END_TAG, XML_NAMESPACE, tag);
        return text;
    }
    private static Question parseQuestion(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, XML_NAMESPACE, XML_QUESTION); // pretty much unnecessary
        Question question = new Question();

        while (parser.next() != XmlPullParser.END_TAG) { // inside 'question' tag
            parser.require(XmlPullParser.START_TAG, XML_NAMESPACE, XML_ANSWER);
            Answer answer = new Answer();

            String subject = parser.getAttributeValue(XML_NAMESPACE, XML_SUBJECT);
            if (subject == null) // if attribute 'subject' does not exist
                throw new XmlPullParserException("expected attribute: " + XML_SUBJECT);
            else
                answer.setSubject(Integer.parseInt(subject));
            boolean text = false, synonyms = false;

            while (parser.next() != XmlPullParser.END_TAG) { // inside 'answer' tag
                if (parser.getEventType() == XmlPullParser.TEXT) {
                    if (text) // if text has already been parsed -> text is split by tags
                        throw new XmlPullParserException("text cannot be split by tags");
                    if (synonyms) // if there are any <synonym> tags
                        throw new XmlPullParserException("text and <synonym> tags cannot co-exist");
                    text = true;
                    answer.getSynonyms().add(parser.getText());
                } else /* (parser.next() != XmlPullParser.START_TAG) */ {
                    String tag = parser.getName();
                    switch (tag) {
                        case XML_SYNONYM:
                            if (text) // if there is any text
                                throw new XmlPullParserException("text and <synonym> tags cannot co-exist");
                            synonyms = true;
                            answer.getSynonyms().add(read(parser, tag));
                            break;
                        case XML_NOTE:
                            answer.setNote(read(parser, tag));
                            break;
                        default:
                            throw new XmlPullParserException("unexpected tag: " + tag);
                    }
                }
            }

            question.getAnswers().add(answer);
            parser.require(XmlPullParser.END_TAG, XML_NAMESPACE, XML_ANSWER);
        }

        parser.require(XmlPullParser.END_TAG, XML_NAMESPACE, XML_QUESTION);
        return question;
    }

    public static Test parse(InputStream stream) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(stream, null);
        Test test = new Test();

        parser.next();
        parser.require(XmlPullParser.START_TAG, XML_NAMESPACE, XML_ROOT);
        while (parser.next() != XmlPullParser.END_TAG) {
            parser.require(XmlPullParser.START_TAG, XML_NAMESPACE, null);
            String tag = parser.getName();
            switch (tag) {
                case XML_ID:
                    test.setID(read(parser, tag));
                    break;
                case XML_NAME:
                    test.setName(read(parser, tag));
                    break;
                case XML_DESCRIPTION:
                    test.setDescription(read(parser, tag));
                    break;
                case XML_CREATOR:
                    test.setAuthor(read(parser, tag));
                    break;
                case XML_TIME_CREATED:
                    test.setTimeCreated(DateTime.parse(read(parser, tag), XML_TIME_FORMAT));
                    break;
                case XML_TIME_MODIFIED:
                    test.setTimeModified(DateTime.parse(read(parser, tag), XML_TIME_FORMAT));
                    break;
                case XML_SUBJECT:
                    test.getSubjects().add(read(parser, tag));
                    break;
                case XML_QUESTION:
                    test.getQuestions().add(parseQuestion(parser));
                    break;
                default:
                    throw new XmlPullParserException("unexpected tag: " + tag);
            }
        }
        parser.require(XmlPullParser.END_TAG, XML_NAMESPACE, XML_ROOT);
        parser.next();
        parser.require(XmlPullParser.END_DOCUMENT, null, null);

        return test;
    }

}
