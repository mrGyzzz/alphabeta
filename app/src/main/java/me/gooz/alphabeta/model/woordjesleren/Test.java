package me.gooz.alphabeta.model.woordjesleren;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import me.gooz.alphabeta.util.AlphanumComparator;
import me.gooz.alphabeta.util.Util;

public class Test extends Item {

    public final static Comparator<Test> COMPARATOR = new Comparator<Test>() {
        public int compare(Test a, Test b) { return Util.compareAll(Util.compare(a.chapter, b.chapter), AlphanumComparator.INSTANCE.compare(a.text, b.text)); }
    };

    public final long chapter;

    Test(long id, String text, long chapter) {
        super(id, text);
        this.chapter = chapter;
    }

    public String toString() {
        return String.format("Test {id=%d, text=\"%s\", chapter=%d}", this.id, this.text, this.chapter);
    }
    //region REGION - Request Functionality
    public static final class TestItem implements Parcelable { // TODO - Temp

        public final String question, answer;

        TestItem(String question, String answer) {
            this.question = question;
            this.answer = answer;
        }

        public String toString() {
            return String.format("%s = %s", this.question, this.answer);
        }
        //region REGION - Parcelable Implementation
        public int describeContents() {
            return 0;
        }
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.question);
            dest.writeString(this.answer);
        }
        protected TestItem(Parcel in) {
            this.question = in.readString();
            this.answer = in.readString();
        }
        public static final Creator<TestItem> CREATOR = new Creator<TestItem>() {
            public TestItem createFromParcel(Parcel source) {
                return new TestItem(source);
            }
            public TestItem[] newArray(int size) {
                return new TestItem[size];
            }
        };
        //endregion

    }
    public List<TestItem> request() throws IOException { // TODO - Temp
        String url = String.format("http://www.woordjesleren.nl/questions.php?chapter=%s", this.id);
        String text = Jsoup.parse(new URL(url), 5000).getElementById("idListPlain").text();
        String[] lines = text.split(Util.LINE_SEPARATOR);

        List<TestItem> test = new ArrayList<>();
        for (String s : lines) {
            String[] parts = s.split("=");
            if (parts.length < 2) continue;
            TestItem item = new TestItem(parts[0], parts[1]);
            test.add(item);
        }

        return test;
    }
    //endregion
    //region REGION - Parcelable Implementation
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(this.chapter);
    }
    protected Test(Parcel in) {
        super(in);
        this.chapter = in.readLong();
    }
    public static final Creator<Test> CREATOR = new Creator<Test>() {
        public Test createFromParcel(Parcel source) {
            return new Test(source);
        }
        public Test[] newArray(int size) {
            return new Test[size];
        }
    };
    //endregion
    //region REGION - SortedListAdapter.ViewModel Implementation
    public <T> boolean isSameModelAs(T t) {
        if (t instanceof Test) {
            Test test = (Test) t;
            return this.id.equals(test.id) && this.text.equals(test.text) && this.chapter == test.chapter;
        }
        return false;
    }
    //endregion

}
