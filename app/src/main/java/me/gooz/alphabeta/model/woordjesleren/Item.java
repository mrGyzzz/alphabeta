package me.gooz.alphabeta.model.woordjesleren;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.core.JsonParser;
import com.github.wrdlbrnft.sortedlistadapter.SortedListAdapter;

import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;

import me.gooz.alphabeta.util.AlphanumComparator;
import me.gooz.alphabeta.util.Util;

public abstract class Item implements Parcelable, Serializable, SortedListAdapter.ViewModel {

    public final static Comparator<Item> COMPARATOR = new Comparator<Item>() {
        public int compare(Item a, Item b) { return AlphanumComparator.INSTANCE.compare(a.text, b.text); }
    };

    public final Long id;
    public final String text;

    Item(String text) {
        this.id = null;
        this.text = text;
    }
    Item(long id, String text) {
        this.id = id;
        this.text = text;
    }

    public String toString() {
        if (this.id == null)
            return String.format("Item {text=\"%s\"}", this.text);
        else
            return String.format("Item {id=%d, text=\"%s\"}", this.id, this.text);
    }
    public boolean equals(Object o) {
        return this.isSameModelAs(o);
    }
    //region REGION - Request Functionality
    static JsonParser createParser(String url) throws IOException {
        return Util.createParser("http://www.woordjesleren.nl/api/" + url + "dataType=json");
    }
    //endregion
    //region REGION - Parcelable Implementation
    public int describeContents() {
        return 0;
    }
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.text);
    }
    protected Item(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.text = in.readString();
    }
    //endregion
    //region REGION - SortedListAdapter.ViewModel Implementation
    public <T> boolean isContentTheSameAs(T t) {
        if (t instanceof Item) {
            Item item = (Item) t;
            return this.text.equals(item.text);
        }
        return false;
    }
    public <T> boolean isSameModelAs(T t) {
        if (this.getClass().equals(t.getClass())) {
            Item item = (Item) t;
            return Util.nullOrEquals(this.id, item.id) && this.text.equals(item.text);
        }
        return false;
    }
    //endregion

}
