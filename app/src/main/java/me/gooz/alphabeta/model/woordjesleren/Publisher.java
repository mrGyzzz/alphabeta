package me.gooz.alphabeta.model.woordjesleren;

import android.os.Parcel;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.gooz.alphabeta.util.Util;

public class Publisher extends Item {

    Publisher(long id, String text) {
        super(id, text);
    }

    public String toString() {
        return super.toString().replaceFirst("Item", "Publisher");
    }
    //region REGION - Request Functionality
    public static final class Response {

        public final Publisher publisher;
        public final List<Book> books;

        Response(Publisher publisher, List<Book> books) {
            this.publisher = publisher;
            this.books = books;
        }

        public String toString() {
            return String.format("Publisher.Response {publisher=%s, books=%s}", this.publisher, Util.toString(this.books));
        }

    }
    public Response request() throws IOException {
        JsonParser parser = Item.createParser("select_books_by_publisher.php?publisher=" + Long.toString(this.id) + "&");

        Util.skipTo(parser, "desc");
        Publisher publisher = new Publisher(this.id, parser.nextTextValue());

        List<Book> books = new ArrayList<>();
        Util.skipTo(parser, JsonToken.START_ARRAY);
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            parser.nextToken();

            long id = parser.nextLongValue(0);
            parser.nextToken();

            String text = parser.nextTextValue();
            parser.nextToken();

            long listCount = parser.nextLongValue(0);
            parser.nextToken();

            Book book = new Book(id, text, listCount);
            books.add(book);
        }

        parser.close();
        return new Response(publisher, books);
    }
    //endregion
    //region REGION - Parcelable Implementation
    protected Publisher(Parcel in) {
        super(in);
    }
    public static final Creator<Publisher> CREATOR = new Creator<Publisher>() {
        public Publisher createFromParcel(Parcel source) {
            return new Publisher(source);
        }
        public Publisher[] newArray(int size) {
            return new Publisher[size];
        }
    };
    //endregion

}
