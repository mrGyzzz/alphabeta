package me.gooz.alphabeta.model.woordjesleren;

import android.os.Parcel;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import me.gooz.alphabeta.App;
import me.gooz.alphabeta.R;
import me.gooz.alphabeta.util.Util;

public class Book extends Item {

    public final long listCount;

    Book(long id, String text, long listCount) {
        super(id, text);
        this.listCount = listCount;
    }

    public String toString() {
        return String.format("Book {id=%d, text=\"%s\", listCount=%d}", this.id, this.text, this.listCount);
    }
    //region REGION - Request Functionality
    public static final class Response {

        public final List<Test> tests;
        public final List<Filter> filters;
        public final Publisher publisher;

        Response(List<Test> tests, List<Filter> filters, Publisher publisher) {
            this.tests = tests;
            this.filters = filters;
            this.publisher = publisher;
        }

        public String toString() {
            return String.format("Book.Response {tests=%s, filters=%s, publisher=%s}", Util.toString(this.tests), Util.toString(this.filters), this.publisher);
        }

    }
    public Response request() throws IOException {
        JsonParser parser = Item.createParser("select_lists.php?book=" + Long.toString(this.id) + "&");

        Set<Filter> filters = new HashSet<>();
        Util.skipTo(parser, JsonToken.START_ARRAY);
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            parser.nextToken();

            parser.nextToken(); // skip 'id'
            parser.nextToken();

            parser.nextToken(); // skip 'text'
            parser.nextToken();

            String chapter = parser.nextTextValue();
            parser.nextToken();

            Filter filter = new Filter(chapter, Filter.Type.CHAPTER);
            filters.add(filter);
        }

        Filter filter = new Filter(App.getInstance().getString(R.string.activity_woordjesleren_tests_recent), Filter.Type.RECENT);
        filters.add(filter);

        Publisher publisher = Util.skipTo(parser, "publisher") ? new Publisher(parser.nextLongValue(0), App.getInstance().getString(R.string.activity_woordjesleren_tests_publisher)) : null;

        parser.close();
        return new Response(null, new ArrayList<>(filters), publisher);
    }
    public Response request(Filter... parameters) throws IOException {
        String url = "select_lists.php?book=" + Long.toString(this.id) + "&";
        for (Filter filter : parameters) url += filter.toParameter();
        JsonParser parser = Item.createParser(url);

        List<Test> tests = new ArrayList<>();
        Util.skipTo(parser, JsonToken.START_ARRAY);
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            parser.nextToken();

            long id = parser.nextLongValue(0);
            parser.nextToken();

            String text = parser.nextTextValue();
            parser.nextToken();

            long chapter = Long.parseLong(parser.nextTextValue());
            parser.nextToken();

            Test test = new Test(id, text.contains(" ") ? text.split(" ", 2)[1] : "", chapter);
            tests.add(test);
        }

        parser.close();
        return new Response(tests, null, null);
    }
    //endregion
    //region REGION - Parcelable Implementation
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeLong(this.listCount);
    }
    protected Book(Parcel in) {
        super(in);
        this.listCount = in.readLong();
    }
    public static final Creator<Book> CREATOR = new Creator<Book>() {
        public Book createFromParcel(Parcel source) {
            return new Book(source);
        }

        public Book[] newArray(int size) {
            return new Book[size];
        }
    };
    //endregion
    //region REGION - SortedListAdapter.ViewModel Implementation
    public <T> boolean isSameModelAs(T t) {
        if (t instanceof Book) {
            Book book = (Book) t;
            return this.id.equals(book.id) && this.text.equals(book.text) && this.listCount == book.listCount;
        }
        return false;
    }
    //endregion

}
