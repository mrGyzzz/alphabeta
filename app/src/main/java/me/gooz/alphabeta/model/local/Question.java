package me.gooz.alphabeta.model.local;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public class Question implements Parcelable {

    private final List<Answer> answers;

    public Question() {
        this.answers = new LinkedList<>();
    }

    protected Question(Parcel in) {
        this();
        in.readList(this.answers, LinkedList.class.getClassLoader());
    }
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.answers);
    }
    public int describeContents() {
        return 0;
    }
    public static final Creator<Question> CREATOR = new Creator<Question>() {
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public List<Answer> getAnswers() {
        return this.answers;
    }



}
