package me.gooz.alphabeta.model.local;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public class Answer {

    private int subject;
    private final List<String> synonyms;
    private String note;

    public Answer() {
        this.synonyms = new LinkedList<>();
    }

    public int getSubject() {
        return this.subject;
    }
    public List<String> getSynonyms() {
        return this.synonyms;
    }
    public String getNote() {
        return this.note;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }
    public void setNote(String note) {
        this.note = note;
    }

}
