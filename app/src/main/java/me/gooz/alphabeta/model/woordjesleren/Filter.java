package me.gooz.alphabeta.model.woordjesleren;

import android.os.Parcel;

public class Filter extends Item {

    public final Type type;

    Filter(String text, Type type) {
        super(text);
        this.type = type;
    }

    public String toParameter() {
        return String.format(this.type.format, this.text);
    }
    public String toString() {
        return String.format("Filter {text=\"%s\", type=%s}", this.text, this.type);
    }
    //region REGION - Parcelable Implementation
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
    }
    protected Filter(Parcel in) {
        super(in);
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : Type.values()[tmpType];
    }
    public static final Creator<Filter> CREATOR = new Creator<Filter>() {
        public Filter createFromParcel(Parcel source) {
            return new Filter(source);
        }
        public Filter[] newArray(int size) {
            return new Filter[size];
        }
    };
    //endregion
    //region REGION - SortedListAdapter.ViewModel Implementation
    public <T> boolean isSameModelAs(T t) {
        if (t instanceof Filter) {
            Filter filter = (Filter) t;
            return this.text.equals(filter.text) && this.type == filter.type;
        }
        return false;
    }
    //endregion

    public enum Type {

          BOOK_DATE(      "date=%s&"  ),
          BOOK_PART(      "part=%s&"  ),
            CHAPTER(   "chapter=%s&"  ),
        SCHOOL_TYPE("schooltype=%s&"  ),
        SCHOOL_YEAR(      "year=%s&"  ),
             RECENT(      "sort=date&");

        private final String format;

        Type(String format) {
            this.format = format;
        }

    }

}
