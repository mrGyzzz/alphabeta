package me.gooz.alphabeta.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.util.Util;

@SuppressWarnings("unused")
public class CorrectView extends CircularProgressBar {

    protected final float pass;
    protected final int color1, color2, color3;
    protected boolean first;

    public CorrectView(Context context, AttributeSet attributesUntyped) {
        super(context, attributesUntyped);

        // initialise fields
        this.max = 1;
        this.text = "-";
        this.pass = 0.55f;
        this.color1 = ContextCompat.getColor(this.getContext(), R.color.red);
        this.color2 = ContextCompat.getColor(this.getContext(), R.color.yellow);
        this.color3 = ContextCompat.getColor(this.getContext(), R.color.green);
        this.first = true;
    }

    public void add(boolean correct) {
        if (this.first) {
            this.first = false;
            this.animate(this.progress + (correct ? 1 : 0), this.max);
        } else
            this.animate(this.progress + (correct ? 1 : 0), this.max + 1);
    }

    protected void onProgressChanged() {
        super.onProgressChanged();
        float f = this.progress / this.max;

        // update text
        int percentage = Math.round(f * 100);
        this.text = Integer.toString(percentage) + "%";

        // adjust text color accordingly
        int color;
        if (f < this.pass) {
            f = f / this.pass;
            color = Util.evaluate(f, this.color1, this.color2);
        } else {
            f = (f - this.pass) / (1 - this.pass);
            color = Util.evaluate(f, this.color2, this.color3);
        }
        this.paintForeground.setColor(color);
    }

    public   float getScore() {
        return this.progress / this.max;
    }
    public boolean isPass() {
        return (this.progress / this.max) >= this.pass;
    }

}
