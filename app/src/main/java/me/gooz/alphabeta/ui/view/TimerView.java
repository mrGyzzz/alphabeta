package me.gooz.alphabeta.ui.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;

import me.gooz.alphabeta.util.Util;

@SuppressWarnings("unused")
public class TimerView extends CircularProgressBar {

    protected final Handler timer;
    protected final Runnable runnable;
    protected int second, nextSecond;
    protected int minute, nextMinute;

    public TimerView(Context context, AttributeSet attributesUntyped) {
        super(context, attributesUntyped);

        // initialise fields
        this.max = 60;
        this.text = ""; // non-null
        this.timer = new Handler();
        this.runnable = new Runnable() {
            public void run() {
                second = nextSecond;
                minute = nextMinute;

                nextSecond++;
                if (nextSecond == max) {
                    nextSecond = 0;
                    nextMinute++;
                }

                animate(nextMinute % 2 == 0 ? nextSecond : max - nextSecond, 0);
                timer.postDelayed(this, 1000);
            }
        };
    }

    public void animate(float progress, float max) {
        // check new value
        progress = (progress < 0) ? (0) : (progress > this.max ? this.max : progress);

        // start animation
        Animator animation = ObjectAnimator.ofFloat(this, "progress", this.progress, progress);
        animation.setDuration(1000);
        animation.setInterpolator(Util.INTERPOLATOR_LINEAR);
        animation.start();
    }
    public void start() {
        this.timer.post(this.runnable);
    }
    public void stop() {
        this.timer.removeCallbacks(this.runnable);
    }

    protected void onDrawText(Canvas canvas) {
        canvas.drawText(String.format("%02d", this.minute), this.pointText.x - 1.25f * this.charWidth, this.pointText.y, this.paintText);
        canvas.drawText(String.format("%02d", this.second), this.pointText.x + 1.25f * this.charWidth, this.pointText.y, this.paintText);
        if (this.second % 2 == 0) canvas.drawText(":", this.pointText.x, this.pointText.y + 0.13f * this.charHeight, this.paintText); // TODO - Is ×0.13 correct?
    }

    public int getSeconds() {
        return this.second;
    }
    public int getMinutes() {
        return this.minute;
    }
    public int getTotalSeconds() {
        return this.second + this.minute * (int) this.max;
    }

}
