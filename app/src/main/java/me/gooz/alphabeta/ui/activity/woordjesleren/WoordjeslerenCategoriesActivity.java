package me.gooz.alphabeta.ui.activity.woordjesleren;

import android.os.Bundle;

import java.io.IOException;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.model.woordjesleren.Category;
import me.gooz.alphabeta.model.woordjesleren.Item;

public class WoordjeslerenCategoriesActivity extends WoordjeslerenActivity<Category> {

    public WoordjeslerenCategoriesActivity() {
        super(Category.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        this.setTitle(this.getString(R.string.activity_woordjesleren_title_categories));

        super.onCreate(savedInstanceState);
    }
    protected void onItemClicked(Category category) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Item.class.getName(), category);
        this.startActivity(WoordjeslerenBooksActivity.class, bundle);
    }
    protected void onWorkerReady() throws IOException {
        this.items = Category.requestAll();
    }

}
