package me.gooz.alphabeta.ui.activity.woordjesleren;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.model.woordjesleren.Book;
import me.gooz.alphabeta.model.woordjesleren.Filter;
import me.gooz.alphabeta.model.woordjesleren.Test;
import me.gooz.alphabeta.ui.activity.main.TestActivity;

public class WoordjeslerenTestsActivity extends WoordjeslerenActivity<Test> {

    private Book book;
    private Filter filter;

    public WoordjeslerenTestsActivity() {
        super(Test.class, Test.COMPARATOR);
    }

    protected void onCreate(Bundle savedInstanceState) {
        this.book = this.getIntent().getParcelableExtra(Book.class.getName());
        this.filter = this.getIntent().getParcelableExtra(Filter.class.getName());
        this.setTitle(this.filter.type == Filter.Type.RECENT ? this.getString(R.string.activity_woordjesleren_title_tests_recent, this.book.text) : this.getString(R.string.activity_woordjesleren_title_tests_chapter, this.book.text, this.filter.text));

        super.onCreate(savedInstanceState);
    }
    protected void onItemClicked(final Test test) {
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                try {
                    List<Test.TestItem> list = test.request();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList(List.class.getName(), (ArrayList<? extends Parcelable>) list);
                    startActivity(TestActivity.class, bundle);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }
    protected void onPerformBind(Holder holder, Test item) {
        String text = item.text.isEmpty() ? this.getString(R.string.activity_woordjesleren_nameless) : item.text;
        if (this.filter.type == Filter.Type.RECENT) text = this.getString(R.string.activity_woordjesleren_chapter_prefix, item.chapter, text);
        holder.textView1.setText(text);
    }
    protected void onWorkerReady() throws IOException {
        this.items = this.book.request(this.filter).tests;
    }

}
