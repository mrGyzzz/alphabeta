package me.gooz.alphabeta.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Build;
import android.util.AttributeSet;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.util.Util;

@SuppressWarnings("unused")
public class CircularProgressBar extends ProgressBar {

    protected final static int DEFAULT_TEXT_COLOR = Color.BLACK;

    protected float sweep;
    protected float charWidth, charHeight;
    protected final PointF pointText;
    protected final Paint paintText;
    protected String text;

    public CircularProgressBar(Context context, AttributeSet attributesUntyped) {
        super(context, attributesUntyped);

        // initialise fields
        this.paintBackground.setAntiAlias(true);
        this.paintForeground.setAntiAlias(true);
        this.paintBackground.setStyle(Paint.Style.STROKE);
        this.paintForeground.setStyle(Paint.Style.STROKE);
        this.pointText = new PointF();
        this.paintText = new Paint();
        this.paintText.setAntiAlias(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) this.paintText.setLetterSpacing(-0.08f); // TODO - Find alternative for <21
        this.paintText.setLinearText(true);
        this.paintText.setSubpixelText(true);
        this.paintText.setTextAlign(Paint.Align.CENTER);
        this.paintText.setTypeface(Util.FONT_ROBOTO_LIGHT);

        // load attributes
        TypedArray attributes = context.obtainStyledAttributes(attributesUntyped, R.styleable.CircularProgressBar);
        this.paintBackground.setStrokeWidth(attributes.getDimension(R.styleable.CircularProgressBar_thickness, ProgressBar.DEFAULT_THICKNESS));
        this.paintForeground.setStrokeWidth(attributes.getDimension(R.styleable.CircularProgressBar_thickness, ProgressBar.DEFAULT_THICKNESS));
        this.paintText.setColor(attributes.getColor(R.styleable.CircularProgressBar_textColor, CircularProgressBar.DEFAULT_TEXT_COLOR));
        this.text = attributes.getString(R.styleable.CircularProgressBar_text);
        attributes.recycle();
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawArc(this.rectfBackground, -90, this.sweep - 360, false, this.paintBackground);
        canvas.drawArc(this.rectfBackground, -90, this.sweep,       false, this.paintForeground);
        if (this.text != null) this.onDrawText(canvas);
    }
    protected void onDrawText(Canvas canvas) {
        canvas.drawText(this.text, this.pointText.x, this.pointText.y, this.paintText);
    }
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);
        int s = (w < h) ? w : h;

        this.setMeasuredDimension(s, s); // force view to square size
    }
    protected void onProgressChanged() {
        this.sweep = this.progress / this.max * 360;
    }
    protected void onSizeChanged(int w, int h, int wOld, int hOld) {
        // adjust bounding box
        float arc = this.paintBackground.getStrokeWidth() / 2;
        this.rectfBackground.left    = this.getPaddingLeft() + arc;
        this.rectfBackground.top     = this.getPaddingTop() + arc;
        this.rectfBackground.right   = w - this.getPaddingRight() - arc;
        this.rectfBackground.bottom  = h - this.getPaddingBottom() - arc;

        // determine text size
        this.paintText.setTextSize(w / 4); // TODO - Replace with real calculation
        this.charWidth   = this.paintText.measureText("0");
        this.charHeight  = this.paintText.ascent() + this.paintText.descent();

        // calculate text position
        this.pointText.x = this.rectfBackground.left + this.rectfBackground.width() / 2;
        this.pointText.y = this.rectfBackground.top + (this.rectfBackground.height() - this.charHeight) / 2;
    }

    public  float getThickness() {
        return this.paintBackground.getStrokeWidth();
    }
    public    int getTextColor() {
        return this.paintText.getColor();
    }
    public String getText() {
        return this.text;
    }

    public void setThickness(float thickness) {
        this.paintBackground.setStrokeWidth(thickness);
        this.paintForeground.setStrokeWidth(thickness);
        this.invalidate();
    }
    public void setTextColor(int color) {
        this.paintText.setColor(color);
        this.invalidate();
    }
    public void setText(String text) {
        this.text = text;
        this.invalidate();
    }

}
