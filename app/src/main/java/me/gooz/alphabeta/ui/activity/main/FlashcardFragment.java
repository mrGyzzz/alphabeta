package me.gooz.alphabeta.ui.activity.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.model.woordjesleren.Test;
import me.gooz.alphabeta.util.Util;

public final class FlashcardFragment extends Fragment {

    private Callback callback;
    private AnimatorSet flipAnimator;

    public void onAttach(Context context) {
        super.onAttach(context);
        this.callback = (Callback) context;
    }
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_flashcard, container, false);
        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = container.getHeight();
                view.setLayoutParams(layoutParams);
            }
        }); // TODO - Replace with something better
        return view;
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // preparation for flip animation
        final View container = this.getView();
        final View cardFront = container.findViewById(R.id.flashcard_front);
        final View cardBack  = container.findViewById(R.id.flashcard_back );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) container.setCameraDistance(container.getCameraDistance() * this.getResources().getDisplayMetrics().density * 1.2f);
        else container.setCameraDistance(8000 * this.getResources().getDisplayMetrics().density);
        cardBack.findViewById(R.id.flashcard_content).setRotationY(180); // mirrors back of card
        cardBack.setAlpha(0f); // hides back of card

        final long duration = Util.ANIMATION_TIME_SHORT;
        Animator flip = ObjectAnimator.ofFloat(container, "rotationY", 0, 180); flip.setDuration(duration);
        Animator hide = ObjectAnimator.ofFloat(cardFront,     "alpha", 1,   0); hide.setDuration(0);
        Animator show = ObjectAnimator.ofFloat(cardBack,      "alpha", 0,   1); show.setDuration(0);

        this.flipAnimator = new AnimatorSet();
        this.flipAnimator.play(flip);
        this.flipAnimator.play(hide).after(duration / 2);
        this.flipAnimator.play(show).with(hide);

        // sets up flashcards
        int i = this.getArguments().getInt(Integer.class.getName());
        TextView headerQuestion = (TextView) cardFront.findViewById(R.id.flashcard_header);
        TextView headerAnswer = (TextView) cardBack.findViewById(R.id.flashcard_header);
        headerQuestion.setText(this.getString(R.string.fragment_flashcard_question, i));
        headerAnswer.setText(this.getString(R.string.fragment_flashcard_answer, i));

        Test.TestItem testItem = this.getArguments().getParcelable(Test.TestItem.class.getName());
        TextView textQuestion = (TextView) cardFront.findViewById(R.id.flashcard_text);
        TextView textAnswer = (TextView) cardBack.findViewById(R.id.flashcard_text);
        textQuestion.setText(testItem.question);
        textAnswer.setText(testItem.answer);

        // sets up animations
        final View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            public boolean up, flipped, flipping;
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_CANCEL:
                        this.up = true;
                        break;
                    case MotionEvent.ACTION_DOWN:
                        this.up = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        this.up = true;
                        if (this.flipped)
                            this.changeListener();
                        else if (!this.flipping) {
                            this.flipping = true;
                            flipAnimator.addListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    flipped = true; // unnecessary to set 'flipping' to 'false', as it's useless now
                                    if (up) changeListener();
                                }
                            });
                            flipAnimator.start();
                        }
                }
                return true;
            }
            public void changeListener() {
                container.setOnTouchListener(new OnFlashcardDismissListener(container, new OnFlashcardDismissListener.Callback() {
                    public void onDismissed(boolean correct) {
                        callback.onDestroy(correct);
                    }
                }));
            }
        };
        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) container.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else container.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                Animation animation = new TranslateAnimation(0, 0, 0 - container.getHeight() - getResources().getDimension(R.dimen.padding_default), 0);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {}
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        container.setOnTouchListener(onTouchListener);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                });
                animation.setDuration(duration);
                animation.setInterpolator(new DecelerateInterpolator());
                container.startAnimation(animation);
            }
        });
    }

    public static FlashcardFragment newInstance(int i, Test.TestItem testItem) {
        Bundle bundle = new Bundle();
        FlashcardFragment fragment = new FlashcardFragment();

        bundle.putInt(Integer.class.getName(), i);
        bundle.putParcelable(Test.TestItem.class.getName(), testItem);

        fragment.setArguments(bundle);
        return fragment;
    }

    public interface Callback {
        void onDestroy(boolean correct);
    }

}
