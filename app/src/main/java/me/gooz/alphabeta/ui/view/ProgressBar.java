package me.gooz.alphabeta.ui.view;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.util.Util;

@SuppressWarnings("unused")
public class ProgressBar extends View {

    protected final static float DEFAULT_THICKNESS  = Util.convertDpToPx(4);
    protected final static float DEFAULT_PROGRESS   =   0;
    protected final static float DEFAULT_MAX        = 100;

    protected final RectF rectfBackground, rectfForeground;
    protected final Paint paintBackground, paintForeground;
    protected float progress, max;

    public ProgressBar(Context context, AttributeSet attributesUntyped) {
        super(context, attributesUntyped);

        // initialise fields
        this.rectfBackground = new RectF();
        this.rectfForeground = new RectF();
        this.paintBackground = new Paint();
        this.paintForeground = new Paint();

        // load attributes
        TypedArray attributes = context.obtainStyledAttributes(attributesUntyped, R.styleable.ProgressBar);
        this.paintBackground.setColor(attributes.getColor(R.styleable.ProgressBar_backgroundColor, Util.getDarkPrimaryColor(this.getContext())));
        this.paintForeground.setColor(attributes.getColor(R.styleable.ProgressBar_foregroundColor, Util.getAccentColor(this.getContext())));
        this.progress = attributes.getFloat(R.styleable.ProgressBar_progress, ProgressBar.DEFAULT_PROGRESS);
        this.max = attributes.getFloat(R.styleable.ProgressBar_max, ProgressBar.DEFAULT_MAX);
        attributes.recycle();
    }

    public void animate(float progress, float max) {
        // check new values
        max = (max < 0) ? (0) : (max);
        progress = (progress < 0) ? (0) : (progress > max ? max : progress);

        // start animation
        if (this.progress == this.max && progress == max) {
            // if going from 100% to 100%, make sure it doesn't move
            this.progress = progress;
            this.max = max;
        } else {
            PropertyValuesHolder animationProgress = PropertyValuesHolder.ofFloat("progress", this.progress, progress);
            PropertyValuesHolder animationMax = PropertyValuesHolder.ofFloat("max", this.max, max);
            ObjectAnimator.ofPropertyValuesHolder(this, animationProgress, animationMax).setDuration(Util.ANIMATION_TIME_LONG).start();
        }
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawRect(this.rectfBackground, this.paintBackground);
        canvas.drawRect(this.rectfForeground, this.paintForeground);
    }
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightDesired = this.getPaddingTop() + Math.round(ProgressBar.DEFAULT_THICKNESS) + this.getPaddingBottom();
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int height = 0;

        switch (heightMode) {
            case MeasureSpec.AT_MOST:
                height = Math.min(heightDesired, heightSize);
                break;
            case MeasureSpec.EXACTLY:
                height = heightSize;
                break;
            case MeasureSpec.UNSPECIFIED:
                height = heightDesired;
                break;
        }

        this.setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), height);
    }
    protected void onProgressChanged() {
        this.rectfForeground.right = this.rectfBackground.left + this.progress / this.max * this.rectfBackground.width();
    }
    protected void onSizeChanged(int w, int h, int wOld, int hOld) {
        this.rectfBackground.left    = this.getPaddingLeft();
        this.rectfBackground.top     = this.getPaddingTop();
        this.rectfBackground.right   = w - this.getPaddingRight();
        this.rectfBackground.bottom  = h - this.getPaddingBottom();

        this.rectfForeground.left    = rectfBackground.left;
        this.rectfForeground.top     = rectfBackground.top;
     // this.rectfForeground.right   = rectfBackground.right;
        this.rectfForeground.bottom  = rectfBackground.bottom;
    }

    public   int getBackgroundColor() {
        return this.paintBackground.getColor();
    }
    public   int getForegroundColor() {
        return this.paintForeground.getColor();
    }
    public float getProgress() {
        return this.progress;
    }
    public float getMax() {
        return this.max;
    }

    public void setBackgroundColor(int color) {
        this.paintBackground.setColor(color);
        this.invalidate();
    }
    public void setForegroundColor(int color) {
        this.paintForeground.setColor(color);
        this.invalidate();
    }
    public void setProgress(float progress) {
        this.progress = (progress < 0) ? (0) : (progress > this.max ? this.max : progress);
        this.onProgressChanged();
        this.invalidate();
    }
    public void setMax(float max) {
        this.max = (max < 0) ? (0) : (max);
        this.onProgressChanged();
        this.invalidate();
    }

}
