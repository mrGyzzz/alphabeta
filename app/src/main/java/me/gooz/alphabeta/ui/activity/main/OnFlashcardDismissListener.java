package me.gooz.alphabeta.ui.activity.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.util.Util;

public final class OnFlashcardDismissListener implements View.OnTouchListener {

    private final VelocityTracker velocityTracker;
    private final Callback callback;
    private final View view;
    private final int baseAnimationTime;
    private final int minFlingVelocity;
    private final float xDistance;
    private float xTranslation, xDown;

    public OnFlashcardDismissListener(View view, Callback callback) {
        this.velocityTracker    = VelocityTracker.obtain();
        this.callback           = callback;
        this.view               = view;
        this.baseAnimationTime  = view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
        this.minFlingVelocity   = ViewConfiguration.get(view.getContext()).getScaledMinimumFlingVelocity();
        this.xDistance          = view.getWidth() + view.getContext().getResources().getDimension(R.dimen.padding_default); // width + padding
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        motionEvent.offsetLocation(this.xTranslation, 0); // because the view is translated during swipe

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                this.velocityTracker.addMovement(motionEvent);
                this.xDown = motionEvent.getRawX();
                break;

            case MotionEvent.ACTION_CANCEL:
                this.reset();
                break;

            case MotionEvent.ACTION_MOVE:
                this.velocityTracker.addMovement(motionEvent);
                this.xTranslation = motionEvent.getRawX() - this.xDown;
                this.view.setTranslationX(this.xTranslation);

                AppCompatImageView image = (AppCompatImageView) this.view.findViewById(R.id.flashcard_back).findViewById(R.id.flashcard_image);
                image.setAlpha(Util.INTERPOLATOR_ACCELERATE.getInterpolation(Math.abs(this.xTranslation) / this.xDistance * 3));
                image.setImageResource(this.xTranslation > 0 ? R.drawable.ic_correct : R.drawable.ic_incorrect);
                break;

            case MotionEvent.ACTION_UP:
                this.velocityTracker.addMovement(motionEvent);
                this.velocityTracker.computeCurrentVelocity(1000);

                boolean dismiss = false;
                boolean toRight = false;
                float  dx = motionEvent.getRawX() - this.xDown;
                float adx = Math.abs(dx);
                float  vx = this.velocityTracker.getXVelocity();
                float avx = Math.abs(vx);

                if (adx > this.xDistance / 2) {
                    dismiss = true;
                    toRight = dx > 0;
                } else if (avx >= this.minFlingVelocity) {
                    dismiss = (vx > 0) == (dx > 0);
                    toRight = vx > 0;
                }

                if (dismiss) {
                    int animationTime = (int) ((this.xDistance - adx) / this.xDistance * this.baseAnimationTime);
                    final boolean correct = toRight; // final is necessary for anonymous class

                    this.velocityTracker.recycle(); // frees memory of velocityTracker
                    this.view.setOnTouchListener(null); // removes this listener, to prevent swiping during animation
                    this.view.findViewById(R.id.flashcard_back).findViewById(R.id.flashcard_image).setAlpha(1);
                    this.view.animate().translationX(toRight ? this.xDistance : -this.xDistance).setDuration(animationTime).setListener(new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator animation) {
                            callback.onDismissed(correct);
                        }
                    });
                } else this.reset();
        }
        return true;
    }
    public void reset() {
        this.velocityTracker.clear();
        this.view.findViewById(R.id.flashcard_back).findViewById(R.id.flashcard_image).setAlpha(0); // hides image
        this.view.animate().translationX(0).setDuration(this.baseAnimationTime); // returns to original position
        this.xTranslation = 0;
    }

    public interface Callback {

        void onDismissed(boolean correct);

    }

}
