package me.gooz.alphabeta.ui.activity.woordjesleren;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import java.io.IOException;

import me.gooz.alphabeta.model.woordjesleren.Book;
import me.gooz.alphabeta.model.woordjesleren.Category;
import me.gooz.alphabeta.model.woordjesleren.Item;
import me.gooz.alphabeta.model.woordjesleren.Publisher;

public class WoordjeslerenBooksActivity extends WoordjeslerenActivity<Book> {

    private Item source;

    public WoordjeslerenBooksActivity() {
        super(Book.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        this.source = this.getIntent().getParcelableExtra(Item.class.getName());
        this.setTitle(this.source.text);

        super.onCreate(savedInstanceState);
    }
    protected void onItemClicked(Book book) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Boolean.class.getName(), this.source instanceof Category);
        this.startActivity(WoordjeslerenChaptersActivity.class, bundle, book);
    }
    protected void onWorkerFinished() {
        this.setTitle(this.source.text); // only really necessary for Publisher
    }
    protected void onWorkerReady() throws IOException {
        if (this.source instanceof Category) {
            Category category = (Category) this.source;
            this.items = category.request();
        } else {
            Publisher publisher = (Publisher) this.source;
            Publisher.Response response = publisher.request();
            this.items = response.books;
            this.source = response.publisher;

            Intent intent = this.getIntent();
            intent.putExtra(Item.class.getName(), (Parcelable) this.source);
            this.setIntent(intent);
        }
    }

}
