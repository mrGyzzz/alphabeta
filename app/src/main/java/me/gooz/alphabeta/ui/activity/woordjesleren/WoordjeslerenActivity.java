package me.gooz.alphabeta.ui.activity.woordjesleren;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.wrdlbrnft.sortedlistadapter.SortedListAdapter;

import org.joda.time.DateTime;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.model.woordjesleren.Book;
import me.gooz.alphabeta.model.woordjesleren.Item;

abstract class WoordjeslerenActivity<T extends Item> extends AppCompatActivity {

    private final Class<T> classT;
    private final Comparator<T> comparator;

    protected SearchView searchView;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected RecyclerView recyclerView;

    protected Adapter<T> adapter;
    protected List<T> items;

    WoordjeslerenActivity(Class<T> classT, Comparator<T> comparator) {
        this.classT = classT;
        this.comparator = comparator;
    }
    WoordjeslerenActivity(Class<T> classT) {
        this(classT, (Comparator<T>) Item.COMPARATOR);
    }

    public     void onBackPressed() {
        if (this.searchView.isIconified())
            super.onBackPressed();
        else
            this.searchView.onActionViewCollapsed();
    }
    protected  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DateTime now = new DateTime(); // TODO - TEMP
        DateTime max = new DateTime(2017, 6, 3, 0, 0);
        if (now.isAfter(max)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Demo Expired")
                   .setMessage("This demo version of Alphabeta has expired, meaning it cannot be used anymore. Please now close the app.")
                   .setOnDismissListener(new DialogInterface.OnDismissListener() {
                       public void onDismiss(DialogInterface dialog) {
                           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                               finishAffinity();
                           else
                               finish();
                       }
                   })
                   .show();
        }

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setContentView(R.layout.activity_woordjesleren);

        this.swipeRefreshLayout = (SwipeRefreshLayout) this.findViewById(R.id.swipe_refresh_layout);
        this.swipeRefreshLayout.setColorSchemeResources(R.color.accent, R.color.primary);
        this.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() { new Worker(WoordjeslerenActivity.this).execute(); }
        });

        this.recyclerView = (RecyclerView) this.findViewById(R.id.recycler_view);
        this.recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        this.adapter = new Adapter<>(this);
        this.adapter.addCallback(new SortedListAdapter.Callback() {
            public void onEditStarted() {}
            public void onEditFinished() {
                recyclerView.scrollToPosition(0);
            }
        });
        this.recyclerView.setAdapter(this.adapter);
        if (savedInstanceState != null) this.items = savedInstanceState.getParcelableArrayList(List.class.getName());
        if (this.items == null) new Worker(this).execute();
        else this.adapter.edit().replaceAll(this.items).commit();
    }
    public  boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.woordjesleren_menu, menu);
        this.searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        this.searchView.setMaxWidth(Integer.MAX_VALUE);
        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                if (items == null) return false;
                List<T> list = new ArrayList<>();
                query = query.toLowerCase();

                for (T item : items)
                    if (item.text.toLowerCase().contains(query))
                        list.add(item);

                adapter.edit().replaceAll(list).commit();
                return true;
            }
            public boolean onQueryTextChange(String query) {
                return this.onQueryTextSubmit(query);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    protected  void onItemClicked(T t) {}
    public  boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) { this.finish(); return true; }
        return super.onOptionsItemSelected(item);
    }
    protected  void onPause() {
        if (this.getIntent().hasExtra(WoordjeslerenActivity.class.getName())) this.overridePendingTransition(0, 0); // if previous activity was also an WoordjeslerenActivity, don't do animations
        super.onPause();
    }
    protected  void onPerformBind(Holder holder, T item) {
        holder.textView1.setText(item.text);
        if (item instanceof Book) {
            Book book = (Book) item;
            holder.textView2.setText(this.getString(R.string.activity_woordjesleren_list_count, book.listCount));
        }
    }
    protected  void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(List.class.getName(), (ArrayList<? extends Parcelable>) this.items);
        super.onSaveInstanceState(outState);
    }
    protected  void onWorkerFinished() {}
    protected  void onWorkerReady() throws IOException {}
    protected  void startActivity(Class<? extends Activity> activity, Bundle bundle, Parcelable... arguments) {
        Intent intent = new Intent(this, activity);
        intent.putExtra(WoordjeslerenActivity.class.getName(), 0).putExtras(bundle);
        for (Parcelable p : arguments) intent.putExtra(p.getClass().getName(), p);

        this.startActivity(intent);
        if (WoordjeslerenActivity.class.isAssignableFrom(activity)) this.overridePendingTransition(0, 0);
    }
    protected  void startActivity(Class<? extends Activity> activity, Parcelable... arguments) {
        this.startActivity(activity, new Bundle() /* I know, not the best for performance */, arguments);
    }

    protected static final class Adapter<T extends Item> extends SortedListAdapter<T> {

        final WeakReference<WoordjeslerenActivity<T>> activity;

        Adapter(WoordjeslerenActivity<T> activity) {
            super(activity, activity.classT, activity.comparator);
            this.activity = new WeakReference<>(activity);
        }

        protected ViewHolder<? extends T> onCreateViewHolder(LayoutInflater layoutInflater, ViewGroup parent, int i) {
            return new Holder<>(layoutInflater.inflate(R.layout.list_item, parent, false), this.activity.get());
        }

    }
    protected static final class Holder<T extends Item> extends SortedListAdapter.ViewHolder<T> {

        final WeakReference<WoordjeslerenActivity<T>> activity;
        final TextView textView1, textView2;

        Holder(View view, WoordjeslerenActivity<T> activity) {
            super(view);
            this.activity = new WeakReference<>(activity);
            this.textView1 = (TextView) view.findViewById(android.R.id.text1);
            this.textView2 = (TextView) view.findViewById(android.R.id.text2);
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) { Holder.this.activity.get().onItemClicked(getCurrentItem()); }
            });
        }

        protected void performBind(T item) {
            this.activity.get().onPerformBind(this, item);
        }

    }
    protected static final class Worker extends AsyncTask<Void, Void, Boolean> {

        final WeakReference<WoordjeslerenActivity> activity;

        Worker(WoordjeslerenActivity activity) {
            this.activity = new WeakReference<>(activity);
        }

        protected Boolean doInBackground(Void... params) {
            try {
                this.activity.get().onWorkerReady();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        protected void onPreExecute() {
            this.activity.get().swipeRefreshLayout.setRefreshing(true);
        }
        protected void onPostExecute(Boolean success) {
            this.activity.get().onWorkerFinished();
            this.activity.get().swipeRefreshLayout.setRefreshing(false);
            if (success)
                this.activity.get().searchView.setQuery(this.activity.get().searchView.getQuery(), true);
            else
                Snackbar.make(this.activity.get().recyclerView, this.activity.get().getString(R.string.error_no_internet), Snackbar.LENGTH_LONG).show();
        }

    }

}
