package me.gooz.alphabeta.ui.activity.main;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.List;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.model.woordjesleren.Test;
import me.gooz.alphabeta.ui.view.CorrectView;
import me.gooz.alphabeta.ui.view.ProgressBar;
import me.gooz.alphabeta.ui.view.TimerView;

public class TestActivity extends AppCompatActivity implements FlashcardFragment.Callback {

    private List<Test.TestItem> list;
    private int current;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_test);
        this.list = this.getIntent().getParcelableArrayListExtra(List.class.getName());
        this.current = -1;
        this.onDestroy(false);

        TimerView timer = (TimerView) this.findViewById(R.id.time_progress);
        timer.start();
    }

    public void onDestroy(boolean correct) {
        this.current++;

        TextView textQuestion = (TextView) this.findViewById(R.id.question_text);
        textQuestion.setText(this.getString(R.string.activity_test_question, this.current, this.list.size()));

        ProgressBar progressQuestion = (ProgressBar) this.findViewById(R.id.question_progress);
        progressQuestion.animate(this.current, this.list.size());

        CorrectView progressCorrect = (CorrectView) this.findViewById(R.id.correct_progress);
        if (this.current != 0) progressCorrect.add(correct);

        if (this.current == this.list.size()) {
            TimerView timer = (TimerView) this.findViewById(R.id.time_progress);
            timer.stop();

            AlertDialog.Builder builder = new AlertDialog.Builder(this); // TODO - TEMP
            builder.setMessage(this.getString(R.string.activity_test_finished, timer.getSeconds(), timer.getMinutes(), (int) progressCorrect.getProgress()))
                   .setPositiveButton("OK", null)
                   .setOnDismissListener(new DialogInterface.OnDismissListener() {
                       public void onDismiss(DialogInterface dialog) {
                           finish();
                       }
                   })
                   .show();

            return;
        }

        FlashcardFragment fragment = FlashcardFragment.newInstance(this.current + 1, this.list.get(this.current));
        this.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_holder, fragment).commit();
    }

}
