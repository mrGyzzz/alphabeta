package me.gooz.alphabeta.ui.activity.woordjesleren;

import android.os.Bundle;

import java.io.IOException;
import java.util.ArrayList;

import me.gooz.alphabeta.R;
import me.gooz.alphabeta.model.woordjesleren.Book;
import me.gooz.alphabeta.model.woordjesleren.Filter;
import me.gooz.alphabeta.model.woordjesleren.Item;
import me.gooz.alphabeta.model.woordjesleren.Publisher;

public class WoordjeslerenChaptersActivity extends WoordjeslerenActivity<Item> {

    private boolean showPublisher;
    private Book book;

    public WoordjeslerenChaptersActivity() {
        super(Item.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        this.showPublisher = this.getIntent().getBooleanExtra(Boolean.class.getName(), true);
        this.book = this.getIntent().getParcelableExtra(Book.class.getName());
        this.setTitle(this.book.text);

        super.onCreate(savedInstanceState);
    }
    protected void onItemClicked(Item item) {
        if (item instanceof Publisher) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Item.class.getName(), item);
            this.startActivity(WoordjeslerenBooksActivity.class, bundle);
        } else
            this.startActivity(WoordjeslerenTestsActivity.class, this.book, item);
    }
    protected void onPerformBind(WoordjeslerenActivity.Holder holder, Item item) {
        String text = item.text;

        if (item instanceof Filter) {
            Filter filter = (Filter) item;
            if (filter.type == Filter.Type.CHAPTER) text = this.getString(R.string.activity_woordjesleren_chapter_item, text);
        }

        holder.textView1.setText(text);
    }
    protected void onWorkerReady() throws IOException {
        Book.Response response = this.book.request();
        this.items = new ArrayList<>(); // I know, not the best for performance
        this.items.addAll(response.filters);
        if (this.showPublisher && response.publisher != null) this.items.add(response.publisher);
    }

}
