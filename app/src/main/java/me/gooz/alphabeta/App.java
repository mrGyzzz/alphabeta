package me.gooz.alphabeta;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static App instance;

    public void onCreate() {
        super.onCreate();
        App.instance = this;
    }
    public static Context getInstance() {
        return App.instance;
    }

}
